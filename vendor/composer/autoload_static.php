<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitddf6e5289108cb9df1d55708b05ab0e6
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Core\\' => 5,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Core\\' => 
        array (
            0 => __DIR__ . '/../..' . '/core',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitddf6e5289108cb9df1d55708b05ab0e6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitddf6e5289108cb9df1d55708b05ab0e6::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitddf6e5289108cb9df1d55708b05ab0e6::$classMap;

        }, null, ClassLoader::class);
    }
}
