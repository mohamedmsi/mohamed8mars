<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends BaseController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function add()
    {
        $salle = SalleModel::all();
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Créneau ajouté avec succès!');
                $this->redirect('creneaux');
            }
        }
        $form = new Form($errors);
        $this->render('app.creneau.add', array(
            'form' =>   $form,
            'salle' =>   $salle,
        ));
    }

    public function index()
    {
        $creneaux = CreneauModel::all();
        $this->render('app.creneau.index', [
            'creneaux' => $creneaux,
        ]);
    }

    public function single($id)
    {
        $creneau = CreneauModel::findById($id);
        if (!$creneau) {
            $this->abort404();
        }
        $this->render('app.creneau.single', [
            'creneau' => $creneau,
        ]);
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['start_at'] = $v->textValid($post['start_at'], 'start_at');
        $errors['nbrehours'] = $v->textValid($post['nbrehours'], 'nbrehours', 1, 24);
        return $errors;
    }
}
