<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('app'); ?>
  </head>
  <body>
  <?php // $view->dump($view->getFlash()) ?>
    <header id="masthead">
      <nav>
          <ul>
              <li><a href="<?= $view->path(''); ?>">Home</a></li>
              <li><a href="<?= $view->path('add-user'); ?>">User</a></li>
              <li><a href="<?= $view->path('add-salle'); ?>">Salle</a></li>
              <li><a href="<?= $view->path('add-creneau'); ?>">Créneau</a></li>
          </ul>
      </nav>
    </header>

    <div class="container">
        <?= $content; ?>

        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $user->nom ?></td>
                    <td><?= $user->email ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <table>
            <thead>
            <tr>
                <th>Titre</th>
                <th>Nombre max d'utilisateurs</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($salles as $salle): ?>
                <tr>
                    <td><?= $salle->title ?></td>
                    <td><?= $salle->maxuser ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Titre de la salle</th>
                <th>Date de début</th>
                <th>Nombre d'heures</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($creneaux as $creneau): ?>
                <tr>
                    <td><?= $creneau->nom ?></td>
                    <td><?= $creneau->title ?></td>
                    <td><?= $creneau->start_at ?></td>
                    <td><?= $creneau->nbrehours ?></td>
                    <td><a href="<?= $view->path('creneau', ['id' => $creneau->id]) ?>">Détails</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>

    <footer id="colophon">
        <div class="wrap">
            <p>MVC 6 - Framework Pédagogique.</p>
        </div>
    </footer>
  <?php echo $view->add_webpack_script('app'); ?>
  </body>
</html>
