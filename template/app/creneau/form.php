<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Salle'); ?>
    <?php echo $form->selectEntity('select-salle', $salle, 'title'); ?>
    <?php echo $form->error('salle'); ?>

    <?php echo $form->label('Début'); ?>
    <?php echo $form->input('start_at','datetime-local', $creneau->start_at ?? ''); ?>
    <?php echo $form->error('start_at'); ?>

    <?php echo $form->label('Durée (en heures)'); ?>
    <?php echo $form->input('nbrehours','number', $creneau->nbrehours ?? ''); ?>
    <?php echo $form->error('nbrehours'); ?>

    <?php echo $form->submit('submitted', 'Ajouter'); ?>
</form>
