<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('nom'); ?>
    <?php echo $form->input('nom','text', $user->nom ?? '') ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('email'); ?>
    <?php echo $form->input('email','email', $user->email ?? '') ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->submit('submitted', 'Ajouter'); ?>
</form>
