<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Titre de la salle'); ?>
    <?php echo $form->input('title','text', $salle->title ?? ''); ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('Nombre maximum d\'utilisateurs'); ?>
    <?php echo $form->input('maxuser','number', $salle->maxuser ?? ''); ?>
    <?php echo $form->error('maxuser'); ?>

    <?php echo $form->submit('submitted', 'Ajouter'); ?>
</form>
