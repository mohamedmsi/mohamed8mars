<?php

$routes = array(
    array('home','default','index'),

    array('user', 'user', 'index'),
    array('add-user', 'user', 'add'),

    array('salle', 'salle', 'index'),
    array('add-salle', 'salle', 'add'),

    array('creneau', 'creneau', 'index'),
    array('single-creneau','creneau','single', array('id')),
    array('add-creneau', 'creneau', 'add'),
);
